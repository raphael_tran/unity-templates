﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* This class is used to add comments on a GameObject in a Scene as a Component. */
[AddComponentMenu("Miscellaneous/Comment")]
public class Comment : MonoBehaviour
{

    [TextArea]
    public string comment = "/* Write your comment here */";


}
