﻿using Gameplay;
using UnityEngine;
using Util;

/*
 * Standard controller which handle well the collisions in a square environnment.
 */
public class Controller2DTopDown0 : BoxRaycaster2D {


	protected override void HandleConstraint(ref Vector2 moveAmount) {

		UpdateRaycaster();

		float maxDistance;

		if (moveAmount.x != 0f) {
			// Horizontal movement
			maxDistance = RaycastSide(
				(moveAmount.x < 0) ? Direction.LEFT : Direction.RIGHT,
				Mathf.Abs(moveAmount.x)
			);
			moveAmount.x = maxDistance * ((moveAmount.x < 0) ? -1 : 1);
		}

		if (moveAmount.y != 0f) {
			// Déplacement vertical
			maxDistance = RaycastSide(
				(moveAmount.y > 0) ? Direction.UP : Direction.DOWN,
				Mathf.Abs(moveAmount.y)
			);
			moveAmount.y = maxDistance * ((moveAmount.y < 0) ? -1 : 1);

		}

	}



}
