﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Util;

public class PauseMenu : MonoBehaviour {

	public static bool gameIsPaused;


	public GameObject pauseMenuUI;

	public string menuScene;


	private void Start() {

	}


	private void Update() {
		if (Input.GetButtonDown("Pause")) {
			if (gameIsPaused) {
				Resume();
			} else {
				Pause();
			}
		}
	}


	public void Pause() {
		Time.timeScale = 0;
		pauseMenuUI.SetActive(true);
		gameIsPaused = true;
	}

	public void Resume() {
		Time.timeScale = 1;
		pauseMenuUI.SetActive(false);
		gameIsPaused = false;
	}


	public void LoadMenu() {
		Debug.Log("Loading Menu");
		Resume();
		SceneManager.LoadScene(menuScene);
	}

	public void Quit() {
		Debug.Log("Quitting");
		Application.Quit();
	}

}
