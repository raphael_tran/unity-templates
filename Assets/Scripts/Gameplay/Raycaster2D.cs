﻿using Gameplay;
using UnityEngine;
using Util;
using DEBUG = Global.DebugParameters;

/*
 * Superclass for all the Controllers 2D which rely on raycasting to detect collisions.
 */
// Note: in a langage handling multiple inheritance, it would be better to be separated from Controller2D.
public abstract class Raycaster2D : Controller2D {

	protected float skinWidth;


	[Tooltip("On which layers will the colliders be detected?")]
	public LayerMask collisionMask;



	/* Draws a ray in the debug view. */
	protected void DrawRay(Vector2 rayOrigin, Vector2 direction, float rayLength) {
		if (DEBUG.RAY_DRAWING_MODE != 0) {
			Debug.DrawRay(
				rayOrigin,
				direction * ((DEBUG.RAY_DRAWING_MODE == 1) ? 1 : rayLength * DEBUG.RAY_LENGTH_MULTIPLIER),
				DEBUG.RAY_COLOR
			);
		}

	}


}
