﻿using System;
using System.Collections;
using UnityEngine;

namespace Util {

/*
 *
 *
 * @author  Raph
 */
public class Fader<T> {



	/*
	 * MEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEHH
	 */
	public IEnumerator ProgressTransition(T initialValue, T targetValue, float transitionTime, Action<float> action) {


		action(0);

		float t = 0;
		while (t < 1) {
			action(Mathf.SmoothStep(0, 1, t));
			yield return null;

			t += Time.deltaTime / transitionTime;
		}
		action(1);

	}







}
}
