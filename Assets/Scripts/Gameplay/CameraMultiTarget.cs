﻿using System.Collections.Generic;
using UnityEngine;

/*
 * A camera script that enable a Camera to follow multiple targets.
 *
 */
[RequireComponent(typeof(Camera))]
public class CameraMultiTarget : MonoBehaviour {

	public List<GameObject> targets;

	[Tooltip("")]
	public Vector2 offset;

	public float smoothingVelocityX;
	public float smoothingVelocityY;
	public float smoothingVelocityZoom;



	private Camera cam;


	private void Start() {
		// Setting up references
		cam = GetComponent<Camera>();

	}


	private void Update() {




	}

}
