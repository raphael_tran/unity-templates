# Unity templates, by Raphaël TRAN


This Unity project is intended to contain many useful elements that tend to be used frequently.
This was initially thought to be a huge time gain in game jams.

Everything is available under a Creative Commons Attribution-ShareAlike License:
https://creativecommons.org/licenses/by-sa/4.0/

Below are the lists of features in developpement and available.



## Features to be added:

* Custom editor script		- I need to learn about it first
* Main menu
* Standard setting menu
* General smoothing function
* Camera Manager
* Camera shake 				- See some GDC...


## Features:


### Audio

* Explosion sound		- always handy...


### Graphics

* Blank pixel sprite	- with some rescaling and color filter, this constitues a quick placeholder
* Invisible sprite	
* Explosion animation	- very important


### Scripts

The main elements of this projects are source code.


#### Gameplay

Scripts related to the gameplay.
Notably contains movement controllers, player representations, camera scripts and an input manager.
See https://drive.google.com/file/d/1Jl7F47Nes2xwZQsZ72t5txx6UpZ1eJvd/view?usp=sharing for the relations between the classes.

*
*
*
*
*



#### Global

This folder contains data that is used globally.

* DebugParameters		- Static class regrouping all parameters related to debug elements
* StandardValues		- 
* GlobalValues			- 

#### UI

UI related scripts.

* ChangeCursorOnHover	- Allow to change the cursor's appearance when hovering over an element
* PauseMenu				- I think the name is quite self-explanatory.

#### Util

Miscellaneous class which provide useful or convenient content.

* Annex			- Static class providing diverse constants and methods
* Comment		- Allow use of comments on a GameObject in the editor
* Direction		- Enumeration of the 4 directions
* UnitInput		- Representation of the set of input related to a unit.



* Controller2D class which manages the movement of GameObjects.
	Standard Controller2D for side-scrollers and top down games are available.
	They are based on raycasting to detect collisions.
* 


