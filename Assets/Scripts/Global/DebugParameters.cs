﻿using UnityEngine;

namespace Global {

	public static class DebugParameters {


		public static readonly Color CAMERA_FOCUS_AREA_COLOR = new Color(1, 0, 0, .5f);


		/*
	 * Mode for the rendering of raycast in the debug view.
	 *
	 * 0 -> Disable ray drawing
	 * 1 -> Only show the direction of the raycast (length are normalized)
	 * 2 -> Show the real rays, with a given length multiplier (see next field)
	 */
		public static readonly int RAY_DRAWING_MODE = 2;

		/*
	 * Length multiplier for the drawing of ray in the debug view.
	 * Only used when RAY_DRAWING_MODE = 2.
	 */
		public static readonly float RAY_LENGTH_MULTIPLIER = 4;

		public static readonly Color RAY_COLOR = Color.green;


	}
}