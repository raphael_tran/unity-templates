﻿using System;
using UnityEngine;
using Util;

namespace Gameplay {

/*
 * The rays are cast from the edge of the BoxCollider2D of a GameObject.
 */
[RequireComponent(typeof(BoxCollider2D))]
public class BoxRaycaster2D : Raycaster2D {

	[Tooltip("Number of horizontal rays")]
	public int rayCountHorizontal;

	[Tooltip("Number of horizontal rays")]
	public int rayCountVertical;


	/*
	 * The four corners of the BoxRaycaster2D (account for the skin)
	 */
	protected BoxRaycaster2DOrigins raycasterOrigins;

	/* The distances between two adjacent rays. */
	protected float raySpacingHorizontal, raySpacingVertical;


	/* Reference to the BoxCollider2D of the GameObject */
	private BoxCollider2D boxCollider;



	protected virtual void Start() {
		// Setting up references
		boxCollider = GetComponent<BoxCollider2D>();

		// Calculating parameters
		Vector2 size = boxCollider.size;
		skinWidth = Global.Const.RAYCASTER_PERCENT_SKIN * Mathf.Min(size.x, size.y);
		raySpacingHorizontal = (size.x - 2*skinWidth) / (rayCountHorizontal - 1);
		raySpacingVertical = (size.y - 2*skinWidth) / (rayCountVertical - 1);

	}


	protected void UpdateRaycaster() {
		Bounds bounds = boxCollider.bounds;

		// Reducing the bounds to account for the skin
		bounds.Expand(skinWidth * -2);

		raycasterOrigins.topLeft = new Vector2(bounds.min.x, bounds.max.y);
		raycasterOrigins.topRight = bounds.max;
		raycasterOrigins.bottomLeft = bounds.min;
		raycasterOrigins.bottomRight = new Vector2(bounds.max.x, bounds.min.y);
	}


	/*
	 * Cast all rays of a given side of the BoxRaycaster2D.
	 * Return the distance to the closest collider detected, or raylength if no collider was hit.
	 * Skin width handling is done internally.
	 * NOTE: will probably be completed to also return the collider found.
	 */
	protected float RaycastSide(Direction side, float rayLength) {

		Vector2 rayOrigin;
		Vector2 direction;

		switch (side) {
		case Direction.UP:
			rayOrigin = raycasterOrigins.topLeft;
			direction = Vector2.up;
			break;
		case Direction.DOWN:
			rayOrigin = raycasterOrigins.bottomLeft;
			direction = Vector2.down;
			break;
		case Direction.LEFT:
			rayOrigin = raycasterOrigins.topLeft;
			direction = Vector2.left;
			break;
		case Direction.RIGHT:
			rayOrigin = raycasterOrigins.topRight;
			direction = Vector2.right;
			break;
		default:
			throw new ArgumentOutOfRangeException(nameof(side), side, "Should not happen if direction has 4 elements");
		}

		rayLength += skinWidth;

		int nbRays;
		Vector2 raySpacing;
		if ((side == Direction.UP) || (side == Direction.DOWN)) {
			nbRays = rayCountHorizontal;
			raySpacing = raySpacingHorizontal * Vector2.right;
		} else if ((side == Direction.LEFT) || (side == Direction.RIGHT)) {
			nbRays = rayCountVertical;
			raySpacing = raySpacingVertical * Vector2.down;

		}
		else {
			throw new Exception("Should not happen if direction has 4 elements");
		}


		for (int i = 0; i < nbRays; i++) {
			RaycastHit2D hit = Physics2D.Raycast(rayOrigin, direction, rayLength, collisionMask);
			DrawRay(rayOrigin, direction, rayLength);

			if (hit) { // If something was hit
				rayLength = hit.distance;
			}

			rayOrigin += raySpacing;
		}

		return rayLength - skinWidth;
	}


	/*
	 * Cast a ray from a given corner of the BoxRaycaster2D and return the distance to the closest collider detected.
	 * Skin width handling is done internally.
	 * NOTE: will probably be completed to also return the collider found.
	 */
	protected float RaycastCorner(int corner, float rayLength) {
		switch (corner) {
		case 0:

			break;
		case 1:

			break;
		case 2:

			break;
		case 3:

			break;
		}


		throw new NotImplementedException();
	}





	/* Defines a rectangle */
	protected struct BoxRaycaster2DOrigins {
		public Vector2 topLeft, topRight;
		public Vector2 bottomLeft, bottomRight;
	}


}
}
