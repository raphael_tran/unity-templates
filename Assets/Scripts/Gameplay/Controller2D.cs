﻿using UnityEngine;

namespace Gameplay {
/**
 * Superclass for all controller scripts.
 * A controller script which inherits from this manages the movement of GameObject while handling constraints, such as
 * collisions.
 * By default, complete any movement without constraint.
 *
 * @author	Raph
 */
public class Controller2D : MonoBehaviour {


	[HideInInspector] public Vector2 velocity;




	/* Simulates the movement of the GameObject this frame according to the current velocity. */
	public void MoveStep() {
		Move(Time.deltaTime * velocity);
	}


	/* Move the GameObject this script is attached to of moveAmount, considering eventual constraints. */
	public void Move(Vector2 moveAmount) {

		HandleConstraint(ref moveAmount);

		// Updating velocity
		velocity = moveAmount / Time.deltaTime;

		// Actual movement
		transform.Translate(moveAmount);

	}


	/*
	 * Update the given move distance according to the constraints and return the new value.
	 * Override this method to specify the behaviour, facing constraints.
	 */
	protected virtual void HandleConstraint(ref Vector2 moveAmount) {
		// Default behaviour: no constraint

	}



}
}
