﻿using System.Collections;
using UnityEngine;

namespace Util {

/*
 * This class stores diverse constants and methods such as math ones.
 */
public static class Annex {

	/*
	 * Interpolates between a and b with smoothing at the limits, in a similar way to Vector2.Lerp.
	 */
	public static Vector2 SmoothStep(Vector2 a, Vector2 b, float t) {

		return new Vector2(
				Mathf.SmoothStep(a.x, b.x, t),
				Mathf.SmoothStep(a.y, b.y, t)
		);
	}


	/*
	 * Interpolates between a and b with smoothing at the limits, in a similar way to Vector3.Lerp.
	 */
	public static Vector3 SmoothStep(Vector3 a, Vector3 b, float t) {

		return new Vector3(
			Mathf.SmoothStep(a.x, b.x, t),
			Mathf.SmoothStep(a.y, b.y, t),
			Mathf.SmoothStep(a.z, b.z, t)
		);
	}



	/*
	 * Won't be used, but I like to have it around...
	 */
	public static float SmootherStep(float t) {
		t = Mathf.Clamp(t, 0, 1);
		return t * t * t * (t * (6*t - 15) + 10);
	}

	public static float SmootherStep(float a, float b, float t) {
		t = Mathf.Clamp(t, 0, 1);
		t = SmootherStep(t);
		return (1-t) * a + t * b;
	}





}

}
