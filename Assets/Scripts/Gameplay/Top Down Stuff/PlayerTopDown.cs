﻿/*
 * The behaviour at the end of a dash.
 * DASH_END_0: Nothing; let the standard deceleration apply (not recommended).
 * DASH_END_1: One frame is used to set the velocity to zero at the end of dash.
 * DASH_END_2: @@@
 */
#define DASH_END_1

using Gameplay;
using UnityEngine;

/*
 * Standard representation of a player character that can move in all directions.
 */
[RequireComponent(typeof(PlayerInput))] [RequireComponent(typeof(Controller2D))]
// ReSharper disable once CheckNamespace
public class PlayerTopDown : PlayerCharacter {


	#region PublicParameters

	[Header("General movement")]

	[Tooltip("Maximum magnitude of the velocity.")]
	public float moveSpeed;

	[Tooltip("The duration needed to reach the maximum speed.")] [Range(0, 1)]
	public float accelerationTime;

	[Tooltip("The duration needed to reach a null velocity from max speed.\n" +
			 "Note that deceleration applies only when there is NO input.")] [Range(0, 1)]
	public float decelerationTime;


	[Header("Dash")]

	[Tooltip("The distance reached by a dash")]
	public float dashDistance;

	[Tooltip("The duration during which the speed is increased for a dash.")]
	public float dashDuration;

	[Tooltip("The duration to wait after a dash is finished to dash again.")]
	public float dashCooldown;

	#endregion


	#region CalculatedParameters

	/* The amount of acceleration applied to the character when moving. */
	private float acceleration;

	/* The amount of decceleration applied constantly to the character. */
	private float deceleration;

	/* The speed applied when performing a dash. */
	private float dashSpeed;

	#endregion


	#region InternalVariables

	/* Used to store the targetVelocity at each frame update. */
	private Vector2 targetVelocity;

	/* Used to store the difference between current velocity and target velocity. */
	private Vector2 deltaVelocity;


	/* The current state regarding the dash. */
	private DashState dashState;

	private Vector2 dashDirection;

	/* Time remaining before dashing state ends. */
	private float remainingDashingTime;

	/* Time remaining before dash is available again. */
	private float currentDashCooldown;

	/* Used to store the amount of movement that will be passed to the controller each frame. */
//	private Vector2 moveAmount;

	#endregion


	#region References

	/* Reference to the Controller2D of this player character. */
	private Controller2D controller;

	/* Reference to the PlayerInput script controlling this player character. */
	private PlayerInput playerInput;

	#endregion


	private void OnValidate() {
		// Calculating physics parameters

		// Acceleration values can be PositiveInfinity and this is okay.
		acceleration = moveSpeed / accelerationTime;
		deceleration = moveSpeed / decelerationTime; // actually the absolute value...

		dashSpeed = dashDistance / dashDuration;

	}


	private void Start() {
		// Setting up references
		controller = GetComponent<Controller2D>();
		playerInput = GetComponent<PlayerInput>();

		// Initializing fields
		dashState = DashState.NEUTRAL;

	}


	private void Update() {

		HandleDash1();

		if (dashState == DashState.DASHING) {
			controller.velocity = dashSpeed * dashDirection;

		} else if (dashState == DashState.DASH_END) {
			// Recovering from dash		(different implementations proposed below)

			#if DASH_END_1
			controller.velocity = Vector2.zero;

			#elif DASH_END_2
			throw new NotImplementedException();

			#endif
			if (dashState != DashState.NEUTRAL) {	// in case neutral state was already set by OnDashRecharge()
				dashState = DashState.COOLDOWN;
			}

		} else {

			StandardUpdateVelocity();

		}

		controller.MoveStep();
	}


	/*
	 * Standard update of the velocity using acceleration and deceleration parameters.
	 */
	private void StandardUpdateVelocity() {

		targetVelocity = moveSpeed * playerInput.directionalInput;
		deltaVelocity = targetVelocity - controller.velocity;

		if (deltaVelocity == Vector2.zero) {	// if target velocity is already reached
			return;
		}

		float currentAcceleration = ((playerInput.directionalInput != Vector2.zero) ? acceleration : deceleration);

		// Note: this is a convenient way to write things, but coordinates can also be dealt with separately.
		controller.velocity = Vector2.Lerp(
			controller.velocity,
			targetVelocity,
			Time.deltaTime * currentAcceleration / deltaVelocity.magnitude
		);

		#region useless
		/*	Here is basically the same code in one line.
			I thought it was fun...

		controller.velocity = Vector2.Lerp(controller.velocity,moveSpeed * input.directionalInput,
				Time.deltaTime * ((input.directionalInput != Vector2.zero) ? acceleration : deceleration) /
				(targetVelocity - controller.velocity).magnitude);

		*/
		#endregion
	}



	/*
	 * First possible implementation of a dash:
	 * the speed is locked at a certain multiplier of the normal maximum speed during a certain duration.
	 *
	 * Note: do not change controller.velocity inside this method as it is then handled in the Update() method.
	 *
	 */
	private void HandleDash1() {
		if (dashState == DashState.NEUTRAL) {
			// Normal state
			if (playerInput.DashInitiated()) {
				OnDashInitiated();
			}

		} else if (dashState == DashState.DASHING) {
			remainingDashingTime -= Time.deltaTime;
			if (remainingDashingTime <= 0) {
				OnDashEnd();
			}

		} else {
			// Dash end or cooldown state

			currentDashCooldown -= Time.deltaTime;

			if (currentDashCooldown <= 0) {
				OnDashRecharge();
			}

		}

	}

	private void OnDashInitiated() {
		dashState = DashState.DASHING;

		dashDirection = (playerInput.directionalInput == Vector2.zero) ?
			playerInput.directionalInputOld : playerInput.directionalInput;
		remainingDashingTime = dashDuration;
		currentDashCooldown = dashCooldown;

		// instanciate particle effect;
		// play sound effect;
		// etc
	}

	private void OnDashEnd() {
		#if DASH_END_0
			// Skipping dash end state
			dashState = DashState.DASH_COOLDOWN;
		#else

		dashState = DashState.DASH_END;

		#endif

	}

	/* When the dash cooldown is finished. */
	private void OnDashRecharge() {
		dashState = DashState.NEUTRAL;
	}


	/* Dash2: instantly teleport to some distance.
	/* Fusion of Dash1 and Dash2 by allowing dash duration to be zero.
	 * -> no, better to have two codes, or use preprocessing directives
	 */




	private enum DashState {
		NEUTRAL,
		DASHING,
		DASH_END,		// used for the frame(s) during which we're decelerating;
		COOLDOWN,

	}



}