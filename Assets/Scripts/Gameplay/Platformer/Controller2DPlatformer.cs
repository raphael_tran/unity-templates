﻿/*
 * SLOPES: if there is a management of slopes.
 */
#define SLOPES

using Gameplay;
using Global;
using UnityEngine;

/**
 * @@@
 *
 * @author Sebastian Lague, arranged by Raph
 *
 */
public class Controller2DPlatformer : BoxRaycaster2D {

	public ContactInformation contactsInfo;



	protected override void Start() {
		base.Start();
		contactsInfo.directionFaced = 1;
	}


	protected override void HandleConstraint(ref Vector2 moveAmount) {

		UpdateRaycaster();

		contactsInfo.Reset();
		contactsInfo.moveAmountOld = moveAmount;


		#if SLOPES
		if (moveAmount.y < 0) {
			DescendSlope(ref moveAmount);
		}
		#endif

		// Horizontal movement
		if (moveAmount.x != 0) {
			contactsInfo.directionFaced = (int) Mathf.Sign(moveAmount.x);
			HandleHorizontalCollisions(ref moveAmount);
		}

		// Vertical movement
		if (moveAmount.y != 0) {
			HandleVerticalCollisions(ref moveAmount);
		}

/*
		if (standingOnMovingGround) {
			contactsInfo.below = true;
		}
*/

	}


	private void HandleHorizontalCollisions(ref Vector2 moveAmount) {

		float directionX = contactsInfo.directionFaced;
		float rayLength = skinWidth + Mathf.Abs(moveAmount.x);

		// XXX
		if (Mathf.Abs(moveAmount.x) < skinWidth) {
			rayLength = 2 * skinWidth;
		}


		for (int i = 0; i < rayCountHorizontal; i++) {
			Vector2 rayOrigin = (directionX == -1) ? raycasterOrigins.topLeft : raycasterOrigins.topRight;
			rayOrigin += Vector2.down * (raySpacingHorizontal * i);
			RaycastHit2D hit = Physics2D.Raycast(
					rayOrigin,
					(directionX == -1) ? Vector2.left : Vector2.right,
					rayLength,
					collisionMask
			);
			DrawRay(rayOrigin, Vector2.right * directionX, rayLength);

			if (hit) {		// If something was hit

				if (hit.distance == 0) {	// If we're actually IN an obstacle
					continue;
				}

				float slopeAngle = Vector2.Angle(hit.normal, Vector2.up);

				if ((i == 0) && (slopeAngle <= Const.MAX_SLOPE_ANGLE)) {
					if (contactsInfo.descendingSlope) {
						contactsInfo.descendingSlope = false;
						moveAmount = contactsInfo.moveAmountOld;
					}
					float distanceToSlopeStart = 0;
					if (slopeAngle != contactsInfo.slopeAngleOld) {		// if we're starting to climb the slope
						distanceToSlopeStart = hit.distance - skinWidth;
						moveAmount.x -= distanceToSlopeStart * directionX;
					}
					ClimbSlope(ref moveAmount, slopeAngle, hit.normal);
					moveAmount.x += distanceToSlopeStart * directionX;
				}

				if (!(contactsInfo.climbingSlope) || (slopeAngle > Const.MAX_SLOPE_ANGLE)) {
					moveAmount.x = (hit.distance - skinWidth) * directionX;

					// Reducing the length of the next rays casted to ignore collisions further than this one
					rayLength = hit.distance;

					if (contactsInfo.climbingSlope) {
						moveAmount.y = Mathf.Tan(contactsInfo.slopeAngle * Mathf.Deg2Rad) * Mathf.Abs(moveAmount.x);
					}

					contactsInfo.left = (directionX == -1);
					contactsInfo.right = (directionX == 1);
				}
			}
		}
	}


	private void HandleVerticalCollisions(ref Vector2 moveAmount) {
		float directionY = Mathf.Sign(moveAmount.y);
		float rayLength = Mathf.Abs(moveAmount.y) + skinWidth;

		for (int i = 0; i < rayCountVertical; i++) {

			Vector2 rayOrigin = (directionY == 1) ? raycasterOrigins.topLeft : raycasterOrigins.bottomLeft;
			rayOrigin += Vector2.right * (raySpacingVertical * i + moveAmount.x);

			RaycastHit2D hit = Physics2D.Raycast(rayOrigin, Vector2.up * directionY, rayLength, collisionMask);
			DrawRay(rayOrigin, Vector2.up * directionY, rayLength);


			if (hit) {
				// Note: slope traversability is not well handled (and it's useless anyway)
				if (hit.collider.CompareTag("Traversable")) {
					if (directionY == 1 || hit.distance == 0) {
						// Moving up through a traversable platform
						continue;
					}
					if (contactsInfo.fallingThroughPlatform) {
						continue;
					}
					/*
					if (input.y == -1) {
						// Initiating fall through traversable platform
						collisions.fallingThroughPlatform = true;
						Invoke("ResetFallingThroughPlatform", .5f);		// Constant value =/
						continue;
					}
					*/
				}

				moveAmount.y = (hit.distance - skinWidth) * directionY;

				// Reducing the length of the next rays casted to ignore collisions further than this one
				rayLength = hit.distance;

				if (contactsInfo.climbingSlope) {
					moveAmount.x = moveAmount.y / Mathf.Tan(contactsInfo.slopeAngle * Mathf.Deg2Rad) * Mathf.Sign(moveAmount.x);
				}

				contactsInfo.below = (directionY == -1);
				contactsInfo.above = (directionY == 1);
			}
		}


		if (contactsInfo.climbingSlope) {
			float directionX = Mathf.Sign(moveAmount.x);
			rayLength = Mathf.Abs(moveAmount.x) + skinWidth;		// The more movement, the longer the rays

			Vector2 rayOrigin = ((directionX == 1) ? raycasterOrigins.bottomRight : raycasterOrigins.bottomLeft) + Vector2.up * moveAmount.y;
			RaycastHit2D hit = Physics2D.Raycast(rayOrigin, Vector2.right * directionX, rayLength, collisionMask);

			if (hit) {
				float slopeAngle = Vector2.Angle(hit.normal, Vector2.up);
				if (slopeAngle != contactsInfo.slopeAngle) {
					moveAmount.x = (hit.distance - skinWidth) * directionX;
					contactsInfo.slopeAngle = slopeAngle;
					contactsInfo.slopeNormal = hit.normal;
				}
			}
		}


	}


	private void ClimbSlope(ref Vector2 moveAmount, float slopeAngle, Vector2 slopeNormal) {

		/* Actually just simple trigonometry */
		float moveDistance = Mathf.Abs(moveAmount.x);
		float climbMoveAmountY = Mathf.Sin(slopeAngle * Mathf.Deg2Rad) * moveDistance;

		if (moveAmount.y <= climbMoveAmountY) {		// If not jumping on slope
			moveAmount.y = climbMoveAmountY;
			moveAmount.x = Mathf.Cos(slopeAngle * Mathf.Deg2Rad) * moveDistance * Mathf.Sign(moveAmount.x);
			contactsInfo.below = true;
			contactsInfo.climbingSlope = true;
			contactsInfo.slopeAngle = slopeAngle;
			contactsInfo.slopeNormal = slopeNormal;
		}
	}


	/*
	 * Manages the descent of slopes.
	 */
	private void DescendSlope(ref Vector2 moveAmount) {

		// Dealing with potential maxAngleSlope

		RaycastHit2D maxSlopeHitLeft = Physics2D.Raycast(
				raycasterOrigins.bottomLeft,
				Vector2.down,
				Mathf.Abs(moveAmount.y) + skinWidth,
			collisionMask
		);
		RaycastHit2D maxSlopeHitRight = Physics2D.Raycast(
				raycasterOrigins.bottomRight,
				Vector2.down,
				Mathf.Abs(moveAmount.y) + skinWidth,
				collisionMask
		);

		if (maxSlopeHitLeft ^ maxSlopeHitRight) { // XOR
			SlideDownMaxSlope(maxSlopeHitLeft, ref moveAmount);
			SlideDownMaxSlope(maxSlopeHitRight, ref moveAmount);
		}

		if (contactsInfo.slidingDownMaxSlope) {
			return;
		}


		// Dealing with potential slope descent

		float directionX = Mathf.Sign(moveAmount.x);
		RaycastHit2D hit = Physics2D.Raycast(
				((directionX == -1) ? raycasterOrigins.bottomRight : raycasterOrigins.bottomLeft),
				Vector2.down,
				Mathf.Infinity,
				collisionMask
		);

		if (!hit) {
			return;
		}

		float slopeAngle = Vector2.Angle(hit.normal, Vector2.up);

		// If no slope to descend or ...
		if ((slopeAngle == 0) | (slopeAngle > Const.MAX_SLOPE_ANGLE) |
				// ... not moving toward descent or ...
				(Mathf.Sign(hit.normal.x) == directionX) |
				// ... IDK???
				(hit.distance - skinWidth > Mathf.Abs(moveAmount.x) * Mathf.Tan(slopeAngle * Mathf.Deg2Rad)))
		{
			return;
		}

		// Adjusting the position on the slope
		moveAmount.x = Mathf.Cos(slopeAngle * Mathf.Deg2Rad) * moveAmount.x;
		moveAmount.y -= Mathf.Sin(slopeAngle * Mathf.Deg2Rad) * Mathf.Abs(moveAmount.x);

		contactsInfo.slopeAngle = slopeAngle;
		contactsInfo.descendingSlope = true;
		contactsInfo.below = true;
		contactsInfo.slopeNormal = hit.normal;

	}


	/*
	 * Update the moveAmount variable if there is a maxAngleSlope to fall.
	 */
	private void SlideDownMaxSlope(RaycastHit2D hit, ref Vector2 moveAmount) {

		if (!hit) {		// if nothing was hit
			return;
		}

		float slopeAngle = Vector2.Angle(hit.normal, Vector2.up);
		if (slopeAngle <= Const.MAX_SLOPE_ANGLE) {
			return;
		}

		moveAmount.x = Mathf.Sign(hit.normal.x) * (Mathf.Abs(moveAmount.y) - hit.distance) /
								Mathf.Tan(slopeAngle * Mathf.Deg2Rad);

		contactsInfo.slopeAngle = slopeAngle;
		contactsInfo.slidingDownMaxSlope = true;
		contactsInfo.slopeNormal = hit.normal;

	}




	/**
	* Information related to the contacts of the GameObject at this moment.
	*/
	public struct ContactInformation {

		// In which directions is there a collision
		public bool above, below;
		public bool left, right;

		// Is the GameObject climbing or descending a slope
		public bool climbingSlope;
		public bool descendingSlope;

		// Is the GameObject currently falling from a maxAngleSlope
		public bool slidingDownMaxSlope;

		// Angle of the slope encountered and previous one
		public float slopeAngle, slopeAngleOld;

		// The normal vector to the slope
		public Vector2 slopeNormal;

		// MoveAmount of the GameObject in the previous frame
		public Vector2 moveAmountOld;

		// Direction faced by the GameObject: -1->left, 1->right
		public int directionFaced;

		// Is the GameObject currently falling through a platform
		public bool fallingThroughPlatform;


		public void Reset() {
			above = below = false;
			left = right = false;

			climbingSlope = false;
			descendingSlope = false;
			slidingDownMaxSlope = false;
			slopeNormal = Vector2.zero;

			slopeAngleOld = slopeAngle;
			slopeAngle = 0;
		}

	}



}
