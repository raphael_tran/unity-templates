﻿using System;
using UnityEngine;

/*
 *
 *
 * @author  Raph
 */
public sealed class GameManager : MonoBehaviour {

		private static readonly GameManager instance = new GameManager();

		/* Explicit static constructor to tell C# compiler
		 * not to mark type as beforefieldinit */
		static GameManager() { }

		private GameManager() { }

		public static GameManager Instance {
			get {
				return instance;
			}
		}



	#region PublicParameters



	#endregion


	#region CalculatedParameters



	#endregion


	#region InternalVariables



	#endregion


	#region References



	#endregion



	private void OnValidate() {
		// Calculating ...

	}

	private void Start() {
		// Setting up references


		// Initializing fields


	}


	private void Update() {


	}



}
