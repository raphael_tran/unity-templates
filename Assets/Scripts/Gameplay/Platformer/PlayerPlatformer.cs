﻿using System;
using UnityEngine;

/**
 * This script represents a player character in a platformer setting.
 * It can jump etc @@@
 * It relies on a Controller2D script which manages the movement.
 *
 * @author Sebastian Lague, arranged by Raph
 */
[RequireComponent(typeof(Controller2DPlatformer))] [RequireComponent(typeof(PlayerInput))]
public class PlayerPlatformer : PlayerCharacter {


	#region PublicParameters

	[Header("General movement")]

	[Tooltip("The maximum horizontal move speed of the player")]
	public float moveSpeed = 6;

	[Tooltip("The maximum jump height, reached when keeping the jump button pressed")]
	public float maxJumpHeight = 4;

	[Tooltip("The minimum height of a jump")]
	public float minJumpHeight = 1;

	[Tooltip("The time (in sec) required to reach the apex of the jump parable")]
	public float timeToJumpApex = .4f;

	[Tooltip("The time required to reach the target horizontal velocity while grounded when starting with a null velocity (set 0 for no inertia)")]
	public float accelerationTimeGrounded = .1f;

	[Tooltip("The time required to completely stop while grounded when starting with max velocity (set 0 for no inertia)")]
	public float decelerationTimeGrounded = .1f;

	[Tooltip("The time required to reach the target horizontal velocity while airborne when starting with a null velocity (set 0 for no inertia)")]
	public float accelerationTimeAirborne = .2f;

	[Tooltip("The time required to reach a null x velocity airborne.")]
	public float decelerationTimeAirborne = .1f;


	[Header("Walljump")]

	[Tooltip("The force applied to jump when wall-jumping toward the wall")]
	public Vector2 wallJumpClimb;

	[Tooltip("The force applied to jump when wall-jumping with no input")]
	public Vector2 wallJumpOff;

	[Tooltip("The force applied to jump when wall-jumping away from the wall")]
	public Vector2 wallLeap;

	[Tooltip("The maximum vertical speed that can be reached when sliding down against a wall")]
	public float wallSlideSpeedMax = 3;

	[Tooltip(" The amount of time the player will stay stuck against a wall when inputing away from it; it is useful to perform wallLeap")]
	public float wallStickTime = .25f;

	#endregion


	#region CalculatedParameters

	/* The constant of gravity applied to this player */
	private float gravity;

	/* The horizontal acceleration applied to the character when moving on a ground. */
	private float accelerationGrounded;

	/* The horizontal deceleration applied to the character when aiming for movement stop. */
	private float decelerationGrounded;

	/* The amount of acceleration applied to the character when moving in the air. */
	private float accelerationAirborne;

	/* The amount of deceleration applied to the character when aiming for movement stop in the air. */
	private float decelerationAirborne;


	/* The vertical velocity which correspond to the heighest jump possible */
	private float maxJumpVelocity;

	/* The vertical velocity which correspond to the lowest jump possible */
	private float minJumpVelocity;

	#endregion


	#region InternalVariables

	/* Used for the smoothing of the horizontal velocity */
	private float velocityXSmoothing;


	/* Is the Player currently wall-sliding */
	private bool wallSliding;

	/* The amount of time remaining before unsticking from a wall */
	private float timeToWallUnstick;

	/* Indicates if the wall is on the left or on the right */
	private int wallDirX;

	#endregion


	#region References

	/* Reference to the Controller2D of this player character. */
	private Controller2DPlatformer controller;

	/* Reference to the PlayerInput script controlling this player character. */
	private PlayerInput playerInput;

	#endregion



	private void OnValidate() {
		// Calculating physics variables according to gameplay parameters
		gravity = -(2 * maxJumpHeight) / (timeToJumpApex * timeToJumpApex);
		maxJumpVelocity = Mathf.Abs(gravity) * timeToJumpApex;
		minJumpVelocity = Mathf.Sqrt(2 * Mathf.Abs(gravity) * minJumpHeight);

		// Acceleration values can be PositiveInfinity and this is okay.		// FIXME: is this true ???
		accelerationGrounded = moveSpeed / accelerationTimeGrounded;
		decelerationGrounded = moveSpeed / decelerationTimeGrounded;	// actually the absolute value...
		accelerationAirborne = moveSpeed / accelerationTimeAirborne;
		decelerationAirborne = moveSpeed / decelerationTimeAirborne;	// same

	}


	private void Start() {
		// Setting up references
		controller = GetComponent<Controller2DPlatformer>();
		playerInput = GetComponent<PlayerInput>();

	}


	private void Update() {

		UpdateVelocity1();
//		HandleWallSliding();

		controller.MoveStep();

		if (controller.contactsInfo.above || controller.contactsInfo.below) {
			if (controller.contactsInfo.slidingDownMaxSlope) {
				// Modulating the vertical acceleration according to the slope
				controller.velocity.y += controller.contactsInfo.slopeNormal.y * -gravity * Time.deltaTime;

			} else {
				controller.velocity.y = 0;		// To avoid "accumulating" gravity when grounded
			}
		}
	}


	/**
	 * Calculate the velocity of the player for this frame according to its horizontal input.
	 * This function uses the SmoothDamp method with acceleration values.
	 * Quite clean, but only one acceleration value and no further control...
	 *
	 * @author	Sebastian Lague
	 */
	private void UpdateVelocity0() {
		controller.velocity.y += gravity * Time.deltaTime;
		controller.velocity.x = Mathf.SmoothDamp(
				controller.velocity.x,
				playerInput.directionalInput.x * moveSpeed,
				ref velocityXSmoothing,
				(controller.contactsInfo.below) ? accelerationTimeGrounded : accelerationTimeAirborne
		);
	}

	/**
	 * Calculate the velocity of the player for this frame according to its horizontal input.
	 * This implementation uses different acceleration and deceleration values when grounded.
	 */
	private void UpdateVelocity1() {

		controller.velocity.y += gravity * Time.deltaTime;


		float targetVelocityX = playerInput.directionalInput.x * moveSpeed;
		float deltaVelocityX = Mathf.Abs(targetVelocityX - controller.velocity.x);

		if (Math.Abs(deltaVelocityX) < float.Epsilon) {	// if target velocityX already reached
			return;
		}

		float currentAcceleration = controller.contactsInfo.below ?
				((playerInput.directionalInput != Vector2.zero) ? accelerationGrounded : decelerationGrounded) :
				((playerInput.directionalInput != Vector2.zero) ? accelerationAirborne : decelerationAirborne);

		controller.velocity.x = Mathf.Lerp(
				controller.velocity.x,
				targetVelocityX,
				currentAcceleration * Time.deltaTime / deltaVelocityX
		);

	}

/*
	private void HandleWallSliding() {

		wallDirX = (controller.collisions.left) ? -1 : 1;
		wallSliding = false;

		if ((controller.collisions.left || controller.collisions.right) && !(controller.collisions.below) && (velocity.y < 0)) {
			wallSliding = true;

			// Clamping the sliding velocity
			if (velocity.y < -wallSlideSpeedMax) {
				velocity.y = -wallSlideSpeedMax;
			}

			if (timeToWallUnstick > 0) {

				// Cancelling horizontal velocity to stay stuck to the wall
				velocityXSmoothing = 0;
				velocity.x = 0;

				if ((directionalInput.x != wallDirX) && (directionalInput.x != 0)) {
					timeToWallUnstick -= Time.deltaTime;
				}
				else {
					timeToWallUnstick = wallStickTime; // Resetting
				}

			} else {
				timeToWallUnstick = wallStickTime; // Resetting
			}

		}

	}


	public void OnJumpInputDown() {

		if (wallSliding) {
			// Performing wall-climb jump
			if (wallDirX == directionalInput.x) {
				velocity.x = -wallDirX * wallJumpClimb.x;
				velocity.y = wallJumpClimb.y;
			}
			// Performing wall jump off
			else if (directionalInput.x == 0) {
				velocity.x = -wallDirX * wallJumpOff.x;
				velocity.y = wallJumpOff.y;
			}
			// Performing wall-leap jump
			else {
				velocity.x = -wallDirX * wallLeap.x;
				velocity.y = wallLeap.y;
			}
		}

		if (controller.collisions.below) { // if grounded
			if (controller.collisions.slidingDownMaxSlope) {
				if (directionalInput.x != -Mathf.Sign(controller.collisions.slopeNormal.x)) { // if not jumping AGAINST max slope
					velocity.y = maxJumpVelocity * controller.collisions.slopeNormal.y;
					velocity.x = maxJumpVelocity * controller.collisions.slopeNormal.x;
				}

			} else {
				// Standard jump
				velocity.y = maxJumpVelocity;

			}
		}
	}

	public void OnJumpInputUp() {
		// This code manages the variable jump height, according to the length of the jump input press
		if (velocity.y > minJumpVelocity) {
			velocity.y = minJumpVelocity;
		}
	}


*/
}