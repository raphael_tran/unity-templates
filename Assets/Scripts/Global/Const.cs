﻿namespace Global {

/*
 * Global constants.
 *
 * @author  Raph
 */
public static class Const {

	/*
	 * The percentage of a Raycaster diameter which constitue skin.
	 */
	public const float RAYCASTER_PERCENT_SKIN = 0.015f;


	/*
	 * The maximum angle (in degree) a player can climb a slope in the platformer environment,
	 * if slopes are enabled.
	 */
	public const float MAX_SLOPE_ANGLE = 65;


}
}
