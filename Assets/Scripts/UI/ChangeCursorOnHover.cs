﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ChangeCursorOnHover : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{

    public Texture2D cursorTexture;
    public Vector2 cursorHotspot = new Vector2(12, 6);   // (12, 6) for the hand



    public void OnPointerEnter(PointerEventData pointerEventData) {
        Cursor.SetCursor(cursorTexture, cursorHotspot, CursorMode.Auto);
    }


    public void OnPointerExit(PointerEventData pointerEventData) {
        Cursor.SetCursor(null, cursorHotspot, CursorMode.Auto);
    }



    public void OnDisable() {
        Cursor.SetCursor(null, cursorHotspot, CursorMode.Auto);
    }



}
