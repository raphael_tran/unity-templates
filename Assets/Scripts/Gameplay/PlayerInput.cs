﻿using System;
using UnityEngine;
using Util;

/*
 * This script manages the player inputs and transmits them to a PlayerCharacter script.
 * You can define an AI with an alternative of this script which send inputs differently.
 */
[RequireComponent(typeof(PlayerCharacter))]
public class PlayerInput : UnitInput {

	public string XAxis = "Horizontal";
	public string YAxis = "Vertical";

	public string dashButton = "Jump";


	public Vector2 directionalInput { get; private set; }

	public Vector2 directionalInputOld { get; private set; }


	private void Update() {

		// I don't like to update this each frame, even if it is not requested, but that is the only way I thought to
		// 	have directionalInputOld.
		directionalInput = new Vector2(Input.GetAxisRaw(XAxis), Input.GetAxisRaw(YAxis)).normalized;
	}


	private void LateUpdate() {
		directionalInputOld = directionalInput;
	}


	public bool DashInitiated() {
		return Input.GetButtonDown(dashButton);
	}
}



